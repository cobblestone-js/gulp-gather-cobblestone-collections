var log = require("fancy-log");
var through = require("through2");
var merge = require("merge");
var _ = require("lodash");

function normalizeSort(include)
{
    // Pull out the sort field or give it a blank one.
    var sorts = include.sort || ["title"];

    // Make sure it is an array to simplify logic.
    if (!Array.isArray(sorts)) {
        sorts = [sorts];
    }

   // Also make sure that every sort item is a duple.
   for (var i = 0; i < sorts.length; i++)
   {
       // If we aren't an array, then make it with a default ascending.
       s = sorts[i];

       if (!Array.isArray(s))
       {
           s = [s, "ascending"];
       }

       // Make sure the second one is lowercase.
       s[1] = s[1].toLowerCase();
       sorts[i] = s;
   }

   // Return the resulting sorts.
   return sorts;
}

function foundSort(include, settings, a, b)
{
    // Loop through the settings.
    for (var setting of settings)
    {
        // Figure out the order we'll be sorting.
        var order = setting[1][0] === "a" ? 1 : -1;

        // See if we have the field.
        var property = setting[0];
        var av = a.page[property];
        var bv = b.page[property];

        // Check the fields for undefined values.
        if (av === undefined && bv === undefined)
            continue;

        if (av === undefined)
            return -1 * order;

        if (bv === undefined)
            return 1 * order;

        // Prepare the values for string comparison.
        if (include.removeArticles)
        {
            av = av.replace(/^(The|An|A)\s+/, "");
            bv = bv.replace(/^(The|An|A)\s+/, "");
        }

        // Compare the values, if they are identical, move to the next one.
        if (av < bv)
            return -1 * order;

        if (av > bv)
            return 1 * order;
    }

    // If all else fails, sort on the relative path.
    if (a.relativePath < b.relativePath)
        return -1;
    else if (a.relativePath > b.relativePath)
        return 1;
    return 0;
}

module.exports = function(params)
{
    // Normalize the parameters.
    params = params || {};
    params.include = params.include || "include";
    params.highWaterMark = params.highWaterMark || 128;

    // The transformation method will go through the input and collect the data
    // so we can resolve the collections in the `flush` method. If the file has
    // a collection themselves, we keep it aside, otherwise we just push it into
    // the stream.
    var files = [];
    var collections = [];

    var transform = function(file, encoding, callback) {
        // Ensure that the file has a collection. This makes our template logic
        // easier to process.
        file.data.collections = file.data.collections || {};

        // Save this file's metadata so we can resolve collections later.
        var data = merge({}, file.data);
        files.push(data);

        // If we don't have a collection, then we're done.
        var includes = file.data.page[params.include];

        if (!includes) return callback(null, file);

        // Force this into an array so we can process multiple ones.
        if (!Array.isArray(includes))
        {
            file.data.page[params.include] = [includes];
        }

        // For files with collection, add them to a list but return nothing so
        // the rest of the stream can process.
        collections.push(file);
        callback();
    };

    // The flush method is what resolves all of the collections. We go through
    // all the collected files and figure out the contents and ordering for each
    // collection.
    var flush = function(done) {
        // Report the collections we are flushing.
        log("collections: Flushing out " + collections.length + " collection pages");

        // Loop through every file in the collection.
        for (var file of collections) {
            // Loop through the includes, building each one in the proper
            // property.
            for (var include of file.data.page[params.include])
            {
                // Search for all the files that are appropriate for this filter.
                var found = [];

                for (var search of files)
                {
                    // If we don't have a page, then skip it because we can't parse
                    // the results.
                    if (!search.page)
                    {
                        continue;
                    }

                    // If we have a search key, then we want to look for that.
                    if (include.key)
                    {
                        // If we don't have the right data, skip it.
                        var pageKey = _.get(search.page, include.key);

                        if (!pageKey)
                        {
                            continue;
                        }

                        // We have the right key, now check to see if the given value is
                        // in either the array or as the string.
                        var keyValue = pageKey;

                        if (!(Array.isArray(keyValue) && keyValue.indexOf(include.value) >= 0) &&
                            (keyValue !== include.value))
                        {
                            continue;
                        }
                    }

                    // If we have directChildren then we want to only include pages
                    // that are the first directly below the current one.
                    if (include.directChildren)
                    {
                        // If the search doesn't start with the current one, skip it.
                        // We also skip ourselves.
                        var searchPath = search.relativePath;

                        if (!searchPath ||
                            !searchPath.startsWith(file.data.relativePath) ||
                            searchPath === file.data.relativePath)
                        {
                            continue;
                        }

                        // Remove the prefix from the search path. If we remove the
                        // trailing "/" from the results, there should be no other
                        // "/" if this is a direct child.
                        searchPath = searchPath.replace(file.data.relativePath, "");
                        searchPath = searchPath.substring(0, searchPath.length - 1);

                        if (searchPath.indexOf("/") >= 0)
                        {
                            continue;
                        }
                    }

                    // Only remove the contents if not requested to keep it.
                    if (!include.includeContents)
                    {
                        search = merge({}, search);
                        delete search.contents;
                    }

                    // If we fell down this far, then include the file.
                    found.push(search);
                }

                // We need to order the results for consistency.
                var sortSettings = normalizeSort(include);

                found.sort(function(a, b) { return foundSort(include, sortSettings, a, b); });

                // If we have a limit, then use that.
                if (include.limit && found.length > include.limit)
                {
                    found = found.splice(0, include.limit);
                }

                // Set the file data.
                include.property = include.property || "includes";
                file.data.collections[include.property] = found;
                //log(
                //    "collections: " +
                //    include.key +
                //    "=" +
                //    include.value +
                //    " => " +
                //    include.property +
                //    ": " +
                //    found.map(f => f.relativePath.replace(/\//, "")).sort().join(", "));
            }

            this.push(file);
        }

        // We are done flushing the results.
        done();
    };

    // Return the resulting object.
    return through(
        { objectMode: true, highWaterMark: params.highWaterMark },
        transform,
        flush);
}
